FROM  openjdk:8-jdk-alpine

LABEL version="1.0.0"
LABEL description="Subjects manager service for skillabb ms practice"
LABEL copyright="skillabb.com"

RUN apk --no-cache add curl

ARG JAR_FILE=target/subjects-management.jar

ADD ${JAR_FILE} /app/subjects-management.jar

CMD ["java", "-Xmx200m", "-jar", "/app/subjects-management.jar"]
