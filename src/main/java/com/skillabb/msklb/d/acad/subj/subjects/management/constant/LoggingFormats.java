package com.skillabb.msklb.d.acad.subj.subjects.management.constant;

public class LoggingFormats {
  
  public static final String ERROR_RESOLVER_RESPONSE = "{} - Error response: {}";
  public static final String ERROR_PARSE_OBJECT_TO_JSON = "Error to parse: {}";
  
  public static final String NEW_REQUEST_INCOMMING = "{} - New request incomming in {}: {}";
  public static final String RESPONSE_OUTGOING = "{} - Response {}: {}";
  public static final String RESPONSE_DELETE_OUTGOING = "{} - Completed delete proccess";
  public static final String RESPONSE_DROP_OUTGOING = "{} - Completed drop proccess";
  
  public static final String ERROR_DEFAUL_MESSAGE = "Error processing the request, for more details check the logs.";
  
  private LoggingFormats() {}
}
