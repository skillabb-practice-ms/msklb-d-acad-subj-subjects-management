package com.skillabb.msklb.d.acad.subj.subjects.management.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.entity.SubjectEntity;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.SubjectResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.UpdateSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.repository.SubjectRepository;
import com.skillabb.msklb.d.acad.subj.subjects.management.service.SubjectService;
import com.skillabb.msklb.d.acad.subj.subjects.management.util.Utility;

@Service
public class SubjectServiceImpl implements SubjectService {

  private final ModelMapper modelMapper;
  private final SubjectRepository subjectRepository;

  public SubjectServiceImpl(final ModelMapper modelMapper,
      final SubjectRepository subjectRepository) {

    this.modelMapper = modelMapper;
    this.subjectRepository = subjectRepository;
  }

  @Override
  public SubjectResponse saveSubject(AddSubjectRequest addSubjectRequest) {
    SubjectEntity subjectEntity = modelMapper.map(addSubjectRequest, SubjectEntity.class);
    subjectEntity
        .setCreatedAt(Utility.convertZonedDateTimeToTimeStamp(addSubjectRequest.getCreatedAt()));
    subjectRepository.save(subjectEntity);
    return modelMapper.map(subjectEntity, SubjectResponse.class);
  }

  @Override
  public SubjectResponse updateSubject(long subjectId, UpdateSubjectRequest updateSubjectRequest) {
    SubjectEntity subjectEntity = subjectRepository.findBySubjectId(subjectId);
    subjectEntity.setName(updateSubjectRequest.getName());
    subjectEntity.setStatus(updateSubjectRequest.getStatus());
    subjectEntity.setAbbreviation(updateSubjectRequest.getAbbreviation());
    subjectEntity.setHours(updateSubjectRequest.getHours());
    subjectEntity.setSchedule(updateSubjectRequest.getSchedule());
    subjectEntity.setProfileType(updateSubjectRequest.getProfileType());
    subjectEntity.setTeacherId(updateSubjectRequest.getTeacherId());
    subjectEntity.setLastModifyBy(updateSubjectRequest.getLastModifyBy());
    subjectEntity.setLastModifyAt(
        Utility.convertZonedDateTimeToTimeStamp(updateSubjectRequest.getLastModifyAt()));
    subjectRepository.save(subjectEntity);
    return modelMapper.map(subjectEntity, SubjectResponse.class);
  }

  @Override
  public List<SubjectResponse> getAllSubjects() {
    List<SubjectEntity> subjectEntity = subjectRepository.findAll();
    List<SubjectResponse> subjectsResponse = new ArrayList<>();
    subjectEntity.stream()
        .forEach(x -> subjectsResponse.add(modelMapper.map(x, SubjectResponse.class)));
    return subjectsResponse;
  }

  @Override
  public SubjectResponse getById(long subjectId) {
    SubjectEntity subjectEntity = subjectRepository.findBySubjectId(subjectId);
    return modelMapper.map(subjectEntity, SubjectResponse.class);
  }

  @Override
  public SubjectResponse getByName(String name) {
    SubjectEntity subjectEntity = subjectRepository.findByName(name);
    return modelMapper.map(subjectEntity, SubjectResponse.class);
  }

  @Override
  public List<SubjectResponse> getbyStatus(long status) {
    List<SubjectEntity> subjectsEntity = subjectRepository.findByStatus(status);
    List<SubjectResponse> subjectsResponse = new ArrayList<>();
    subjectsEntity.stream()
        .forEach(x -> subjectsResponse.add(modelMapper.map(x, SubjectResponse.class)));
    return subjectsResponse;
  }

  @Override
  public SubjectResponse getByAbbreviation(String abbreviation) {
    SubjectEntity subjectEntity = subjectRepository.findByAbbreviation(abbreviation);
    return modelMapper.map(subjectEntity, SubjectResponse.class);
  }

  @Override
  public List<SubjectResponse> getByProfileType(long profileType) {
    List<SubjectEntity> subjectEntities = subjectRepository.findByProfileType(profileType);
    List<SubjectResponse> subjectResponses = new ArrayList<>();
    subjectEntities.stream()
        .forEach(x -> subjectResponses.add(modelMapper.map(x, SubjectResponse.class)));
    return subjectResponses;
  }

  @Override
  public List<SubjectResponse> getByTeacherId(long teacherId) {
    List<SubjectEntity> subjectEntities = subjectRepository.findByTeacherId(teacherId);
    List<SubjectResponse> subjectResponses = new ArrayList<>();
    subjectEntities.stream()
        .forEach(x -> subjectResponses.add(modelMapper.map(x, SubjectResponse.class)));
    return subjectResponses;
  }

  @Override
  public void deleteById(long subjectId) {
    SubjectEntity subjectEntity = subjectRepository.findBySubjectId(subjectId);
    subjectEntity.setStatus(SubjectConstants.VALUE_INT_NEGATIVE_1);
    subjectRepository.save(subjectEntity);
  }

  @Override
  public void dropById(long subjectId) {
    subjectRepository.deleteById(subjectId);
  }

}
