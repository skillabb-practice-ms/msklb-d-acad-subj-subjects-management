package com.skillabb.msklb.d.acad.subj.subjects.management.model;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubjectResponse {

  private long subjectId;

  private String name;

  private long status;

  private String abbreviation;

  private long hours;

  private String schedule;

  private long profileType;

  private long teacherId;

  private long createdBy;

  private Timestamp createdAt;

  private long lastModifyBy;

  private Timestamp lastModifyAt;

}
