package com.skillabb.msklb.d.acad.subj.subjects.management.constant;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@EnableConfigurationProperties
@Getter
public class SubjectProperties {

  public static final String BASE_PATH = "${constant.uri.base-path}";
  public static final String SAVE_SUBJECT = "${constant.uri.specific-path.save}";
  public static final String UPDATE_SUBJECT = "${constant.uri.specific-path.update}";
  public static final String QUERY_FIND_ALL = "${constant.uri.specific-path.query.find-all}";
  public static final String QUERY_FIND_BY_ID = "${constant.uri.specific-path.query.find-by-id}";
  public static final String QUERY_FIND_BY_NAME =
      "${constant.uri.specific-path.query.find-by-name}";
  public static final String QUERY_FIND_BY_STATUS =
      "${constant.uri.specific-path.query.find-by-status}";
  public static final String QUERY_FIND_BY_ABBREVIATION =
      "${constant.uri.specific-path.query.find-by-abbreviation}";
  public static final String QUERY_FIND_BY_PROFILE_TYPE =
      "${constant.uri.specific-path.query.find-by-profile-type}";
  public static final String QUERY_FIND_BY_TEACHER_ID =
      "${constant.uri.specific-path.query.find-by-teacher-id}";
  public static final String DELETE_BY_ID = "${constant.uri.specific-path.delete.delete-by-id}";
  public static final String DROP_BY_ID = "${constant.uri.specific-path.drop.eliminated}";



  public static final String SAVE_OPERATION_SUBJECT = "${constant.operation.specific-path.save}";
  public static final String UPDATE_OPERATION_SUBJECT =
      "${constant.operation.specific-path.update}";
  public static final String QUERY_OPERATION_FIND_ALL =
      "${constant.operation.specific-path.find-all}";
  public static final String QUERY_OPERATION_FIND_BY_ID =
      "${constant.operation.specific-path.find-by-id}";
  public static final String QUERY_OPERATION_FIND_BY_NAME =
      "${constant.operation.specific-path.find-by-name}";
  public static final String QUERY_OPERATION_FIND_BY_STATUS =
      "${constant.operation.specific-path.find-by-status}";
  public static final String QUERY_OPERATION_FIND_BY_ABBREVIATION =
      "${constant.operation.specific-path.find-by-abbreviation}";
  public static final String QUERY_OPERATION_FIND_BY_PROFILE_TYPE =
      "${constant.operation.specific-path.find-by-profile-type}";
  public static final String QUERY_OPERATION_FIND_BY_TEACHER_ID =
      "${constant.operation.specific-path.find-by-teacher-id}";
  public static final String DELETE_OPERATION_BY_ID =
      "${constant.operation.specific-path.delete-by-id}";
  public static final String DROP_OPERATION_BY_ID =
      "${constant.operation.specific-path.eliminated}";

  private SubjectProperties() {};
}
