package com.skillabb.msklb.d.acad.subj.subjects.management.error;

import com.skillabb.msklb.d.acad.subj.subjects.management.constant.ApiConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.LoggingFormats;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.util.Utility;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import lombok.extern.slf4j.Slf4j;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

@Slf4j
@RestControllerAdvice
public class ErrorResolver {

  private ErrorResponse makeErrorResponse(String type, String code, String details, String location,
      String moreInfo, String uuid) {
    return new ErrorResponse(type, code, details, location, moreInfo, uuid, ZonedDateTime.now());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveMethodArgumentNotValidException(HttpServletRequest request,
      MethodArgumentNotValidException ex) {
    List<ObjectError> errors = ex.getAllErrors();
    List<String> errrorDetails = new ArrayList<>();
    errors.stream().forEach(x -> errrorDetails.add(((FieldError) x).getField()
        + ApiConstants.COMMA_SEPARATOR + SubjectConstants.SPACE + x.getDefaultMessage()));

    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            Utility.getJson(errrorDetails), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ErrorResponse resolveNoHandlerFoundException(HttpServletRequest request,
      NoHandlerFoundException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpRequestMethodNotSupportedException(HttpServletRequest request,
      HttpRequestMethodNotSupportedException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.INVALID.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMediaTypeNotAcceptableException(HttpServletRequest request,
      HttpMediaTypeNotAcceptableException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorRespose =
        makeErrorResponse(ErrorType.INVALID.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorRespose));
    return errorRespose;
  }

  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMediaTypeNotSupportedException(HttpServletRequest request,
      HttpMediaTypeNotSupportedException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.INVALID.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(ServletRequestBindingException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveServletRequestBindingException(HttpServletRequest request,
      ServletRequestBindingException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMessageNotReadableException(HttpServletRequest request,
      HttpMessageNotReadableException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveConstraintViolationException(HttpServletRequest request,
      ConstraintViolationException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    return errorResponse;
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse resolveDataIntegrityViolationException(HttpServletRequest request,
      DataIntegrityViolationException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse = makeErrorResponse(ErrorType.FATAL.name(),
        ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR, ex.getMessage(),
        request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    errorResponse.setDetails(LoggingFormats.ERROR_DEFAUL_MESSAGE);
    return errorResponse;
  }

  @ExceptionHandler(DataAccessException.class)
  @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
  public ErrorResponse resolveDataAccessException(HttpServletRequest request,
      DataAccessException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.FATAL.name(), ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    errorResponse.setDetails(LoggingFormats.ERROR_DEFAUL_MESSAGE);
    return errorResponse;
  }

  @ExceptionHandler(DataAccessResourceFailureException.class)
  @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
  public ErrorResponse resolveDataAccessResourceFailureException(HttpServletRequest request,
      DataAccessResourceFailureException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.FATAL.name(), ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    errorResponse.setDetails(LoggingFormats.ERROR_DEFAUL_MESSAGE);
    return errorResponse;
  }

  @ExceptionHandler(InvalidDataAccessApiUsageException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ErrorResponse resolveInvalidDataAccessApiUsageException(HttpServletRequest request,
      InvalidDataAccessApiUsageException ex) {
    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse =
        makeErrorResponse(ErrorType.ERROR.name(), ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
            ex.getMessage(), request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    errorResponse.setDetails(LoggingFormats.ERROR_DEFAUL_MESSAGE);
    return errorResponse;
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse resolveException(HttpServletRequest request, Exception ex) {

    String uuid = request.getHeader(ApiConstants.UUID_HEADER);

    ErrorResponse errorResponse = makeErrorResponse(ErrorType.FATAL.name(),
        ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR, ex.getMessage(),
        request.getServletPath(), SubjectConstants.EMPTY, uuid);
    log.error(LoggingFormats.ERROR_RESOLVER_RESPONSE, uuid, Utility.getJson(errorResponse));
    errorResponse.setDetails(LoggingFormats.ERROR_DEFAUL_MESSAGE);
    return errorResponse;
  }
}
