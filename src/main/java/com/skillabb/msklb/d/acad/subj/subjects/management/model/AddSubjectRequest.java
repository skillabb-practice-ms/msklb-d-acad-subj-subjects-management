package com.skillabb.msklb.d.acad.subj.subjects.management.model;

import java.time.ZonedDateTime;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.ValidationMessages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddSubjectRequest {

  @NotNull(message = ValidationMessages.MUST_BE_NOT_NULL)
  @NotEmpty(message = ValidationMessages.MUST_BE_NOT_EMPTY)
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_200,
      message = ValidationMessages.SIZE_MIN_X_MAX_X)
  private String name;

  @Positive(message = ValidationMessages.POSITIVE_VALUE)
  private long status;

  @NotNull(message = ValidationMessages.MUST_BE_NOT_NULL)
  @NotEmpty(message = ValidationMessages.MUST_BE_NOT_EMPTY)
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_15,
      message = ValidationMessages.SIZE_MIN_X_MAX_X)
  private String abbreviation;

  @Positive(message = ValidationMessages.POSITIVE_VALUE)
  private long hours;

  @NotNull(message = ValidationMessages.MUST_BE_NOT_NULL)
  @NotEmpty(message = ValidationMessages.MUST_BE_NOT_EMPTY)
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_100,
      message = ValidationMessages.SIZE_MIN_X_MAX_X)
  private String schedule;

  @Positive(message = ValidationMessages.POSITIVE_VALUE)
  private long profileType;

  @Positive(message = ValidationMessages.POSITIVE_VALUE)
  private long teacherId;

  @Positive(message = ValidationMessages.POSITIVE_VALUE)
  private long createdBy;

  @NotNull(message = ValidationMessages.MUST_BE_NOT_NULL)
  private ZonedDateTime createdAt;

}
