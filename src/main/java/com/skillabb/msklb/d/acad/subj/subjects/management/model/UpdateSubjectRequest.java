package com.skillabb.msklb.d.acad.subj.subjects.management.model;

import java.time.ZonedDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.skillabb.msklb.d.acad.subj.subjects.management.constant.ApiConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateSubjectRequest {

  @NotNull
  @NotEmpty
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_200)
  private String name;

  @NotNull
  private long status;

  @NotNull
  @NotEmpty
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_15)
  private String abbreviation;

  @NotNull
  private long hours;

  @NotNull
  @NotEmpty
  @Size(min = SubjectConstants.VALUE_INT_1, max = SubjectConstants.VALUE_INT_100)
  private String schedule;

  @NotNull
  private long profileType;

  @NotNull
  private long teacherId;

  @NotNull
  private long lastModifyBy;

  @NotNull
  @Pattern(regexp = ApiConstants.FORMAT_ERROR_RESPONSE)
  private ZonedDateTime lastModifyAt;

}
