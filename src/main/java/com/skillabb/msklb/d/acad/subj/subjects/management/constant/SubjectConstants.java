package com.skillabb.msklb.d.acad.subj.subjects.management.constant;

public class SubjectConstants {

  public static final String EMPTY = "";
  public static final String SPACE = " ";

  public static final String COMPONENT_SCAN = "com.skillabb.*";

  public static final String SUBJECT_TABLE_NAME = "SUBJECT";
  
  public static final String SUBJECT_SEQUENCE_ENTITY_NAME = "SUBJECT_SEQUENCE";
  public static final String SUBJECT_SEQUENCE_ID_NAME = "SUBJECT_SEQUENCE_ID";

  public static final String SUBJECT_ID_COLUMN = "SUBJECT_ID";
  public static final String NAME_COLUMN = "NAME";
  public static final String STATUS_COLUMN = "STATUS";
  public static final String ABBREVIATION_COLUMN = "ABBREVIATION";
  public static final String HOURS_COLUMN = "HOURS";
  public static final String SCHEDULE_COLUMN = "SCHEDULE";
  public static final String PROFILE_TYPE_COLUMN = "PROFILE_TYPE";
  public static final String TEACHER_ID_COLUMN = "TEACHER_ID";
  public static final String CREATE_BY_COLUMN = "CREATE_BY";
  public static final String CREATE_AT_COLUMN = "CREATE_AT";
  public static final String LAST_MODIFY_BY_COLUMN = "LAST_MODIFY_BY";
  public static final String LAST_MODYFY_AT_COLUMN = "LAST_MODYFY_AT";

  public static final int VALUE_INT_NEGATIVE_1 = -1;
  public static final int VALUE_INT_1 = 1;
  public static final int VALUE_INT_15 = 15;
  public static final int VALUE_INT_100 = 100;
  public static final int VALUE_INT_200 = 200;


  private SubjectConstants() {}
}
