package com.skillabb.msklb.d.acad.subj.subjects.management.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = SubjectConstants.SUBJECT_TABLE_NAME)
public class SubjectEntity {

  @Id
  @Column(name = SubjectConstants.SUBJECT_ID_COLUMN)
  @SequenceGenerator(name = SubjectConstants.SUBJECT_SEQUENCE_ENTITY_NAME, 
    sequenceName = SubjectConstants.SUBJECT_SEQUENCE_ID_NAME, 
    initialValue = SubjectConstants.VALUE_INT_1, allocationSize = SubjectConstants.VALUE_INT_1)
  @GeneratedValue(strategy = GenerationType.AUTO, 
    generator = SubjectConstants.SUBJECT_SEQUENCE_ENTITY_NAME)
  private long subjectId;

  @Column(name = SubjectConstants.NAME_COLUMN)
  private String name;

  @Column(name = SubjectConstants.STATUS_COLUMN)
  private long status;

  @Column(name = SubjectConstants.ABBREVIATION_COLUMN)
  private String abbreviation;

  @Column(name = SubjectConstants.HOURS_COLUMN)
  private long hours;

  @Column(name = SubjectConstants.SCHEDULE_COLUMN)
  private String schedule;

  @Column(name = SubjectConstants.PROFILE_TYPE_COLUMN)
  private long profileType;

  @Column(name = SubjectConstants.TEACHER_ID_COLUMN)
  private long teacherId;

  @Column(name = SubjectConstants.CREATE_BY_COLUMN)
  private long createdBy;

  @Column(name = SubjectConstants.CREATE_AT_COLUMN)
  private Timestamp createdAt;

  @Column(name = SubjectConstants.LAST_MODIFY_BY_COLUMN)
  private long lastModifyBy;

  @Column(name = SubjectConstants.LAST_MODYFY_AT_COLUMN)
  private Timestamp lastModifyAt;

}
