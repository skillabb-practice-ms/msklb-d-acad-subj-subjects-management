package com.skillabb.msklb.d.acad.subj.subjects.management.error;

import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.ApiConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.util.Utility;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

  @NotNull
  private String type;
  @NotNull
  private String code;
  private String details;
  private String location;
  private String moreInfo;
  @NotNull
  private String uuid;
  private ZonedDateTime timestamp;

  public String getType() {
    return Utility.isEmpty(type) ? SubjectConstants.EMPTY : type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return Utility.isEmpty(code) ? SubjectConstants.EMPTY : code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDetails() {
    return Utility.isEmpty(details) ? SubjectConstants.EMPTY : details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public String getLocation() {
    return Utility.isEmpty(location) ? SubjectConstants.EMPTY : location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getMoreInfo() {
    return Utility.isEmpty(moreInfo) ? SubjectConstants.EMPTY : moreInfo;
  }

  public void setMoreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
  }

  public String getUuid() {
    return Utility.isEmpty(uuid) ? SubjectConstants.EMPTY : uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = ApiConstants.FORMAT_ERROR_RESPONSE)
  public ZonedDateTime getTimestamp() {
    return timestamp == null ? ZonedDateTime.now() : timestamp;
  }

  public void setTimestamp(ZonedDateTime timestamp) {
    this.timestamp = timestamp;
  }
}
