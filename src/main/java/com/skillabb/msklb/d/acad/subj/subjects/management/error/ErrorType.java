package com.skillabb.msklb.d.acad.subj.subjects.management.error;

public enum ErrorType {
  
  ERROR, WARN, INVALID, FATAL
  
}
