package com.skillabb.msklb.d.acad.subj.subjects.management.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.entity.SubjectEntity;

@Repository
public interface SubjectRepository extends JpaRepository<SubjectEntity, Long> {

  SubjectEntity findBySubjectId(@Param(SubjectConstants.SUBJECT_ID_COLUMN) long subjectId);

  SubjectEntity findByName(@Param(SubjectConstants.NAME_COLUMN) String name);

  List<SubjectEntity> findByStatus(@Param(SubjectConstants.STATUS_COLUMN) long status);

  SubjectEntity findByAbbreviation(
      @Param(SubjectConstants.ABBREVIATION_COLUMN) String abbreviation);

  List<SubjectEntity> findByProfileType(
      @Param(SubjectConstants.PROFILE_TYPE_COLUMN) long profileType);

  List<SubjectEntity> findByTeacherId(@Param(SubjectConstants.TEACHER_ID_COLUMN) long teacherId);

}
