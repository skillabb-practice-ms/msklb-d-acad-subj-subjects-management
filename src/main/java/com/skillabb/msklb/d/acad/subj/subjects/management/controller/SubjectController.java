package com.skillabb.msklb.d.acad.subj.subjects.management.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.skillabb.msklb.d.acad.subj.subjects.management.constant.ApiConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.LoggingFormats;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectProperties;
import com.skillabb.msklb.d.acad.subj.subjects.management.error.ErrorResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.SubjectResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.UpdateSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.service.SubjectService;
import com.skillabb.msklb.d.acad.subj.subjects.management.util.Utility;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Validated
@RequestMapping(value = SubjectProperties.BASE_PATH)
public class SubjectController {

  private final SubjectService subjectService;

  public SubjectController(final SubjectService subjectService) {
    super();
    this.subjectService = subjectService;
  }

  @Operation(summary = SubjectProperties.SAVE_OPERATION_SUBJECT)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_CREATED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = SubjectResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @PutMapping(value = SubjectProperties.SAVE_SUBJECT)
  public ResponseEntity<SubjectResponse> addSubject(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @RequestBody @Valid AddSubjectRequest addSubjectRequest) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid, SubjectProperties.SAVE_OPERATION_SUBJECT,
        Utility.getJson(addSubjectRequest));
    SubjectResponse response = subjectService.saveSubject(addSubjectRequest);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid, SubjectProperties.SAVE_OPERATION_SUBJECT,
        Utility.getJson(response));
    return new ResponseEntity<>(response, HttpStatus.CREATED);
  }

  @Operation(summary = SubjectProperties.UPDATE_OPERATION_SUBJECT)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = SubjectResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @PostMapping(value = SubjectProperties.UPDATE_SUBJECT)
  public ResponseEntity<SubjectResponse> updateSubject(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long subjectId, @RequestBody @Valid UpdateSubjectRequest updateSubjectRequest) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid, SubjectProperties.UPDATE_OPERATION_SUBJECT,
        Utility.getJson(updateSubjectRequest));
    SubjectResponse subjectResponse = subjectService.updateSubject(subjectId, updateSubjectRequest);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid, SubjectProperties.UPDATE_OPERATION_SUBJECT,
        Utility.getJson(subjectResponse));
    return new ResponseEntity<>(null, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_ALL)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              array = @ArraySchema(schema = @Schema(implementation = SubjectResponse.class)))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_ALL)
  public ResponseEntity<List<SubjectResponse>> findAll(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid, SubjectProperties.QUERY_OPERATION_FIND_ALL,
        SubjectProperties.QUERY_OPERATION_FIND_ALL);
    List<SubjectResponse> subjectsResponse = subjectService.getAllSubjects();
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid, SubjectProperties.QUERY_OPERATION_FIND_ALL,
        Utility.getJson(subjectsResponse));
    return new ResponseEntity<>(subjectsResponse, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_ID)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = SubjectResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_ID)
  public ResponseEntity<SubjectResponse> findById(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long subjectId) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_ID, subjectId);
    SubjectResponse response = subjectService.getById(subjectId);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid, SubjectProperties.QUERY_OPERATION_FIND_BY_ID,
        Utility.getJson(response));
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_NAME)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = SubjectResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_NAME)
  public ResponseEntity<SubjectResponse> findByName(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable String name) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_NAME, name);
    SubjectResponse response = subjectService.getByName(name);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid, SubjectProperties.QUERY_OPERATION_FIND_BY_NAME,
        Utility.getJson(response));
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_STATUS)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              array = @ArraySchema(schema = @Schema(implementation = SubjectResponse.class)))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_STATUS)
  public ResponseEntity<List<SubjectResponse>> findByStatus(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long status) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_STATUS, status);
    List<SubjectResponse> subjectResponses = subjectService.getbyStatus(status);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_STATUS, Utility.getJson(subjectResponses));
    return new ResponseEntity<>(subjectResponses, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_ABBREVIATION)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = SubjectResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_ABBREVIATION)
  public ResponseEntity<SubjectResponse> findByAbbreviation(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable String abbreviation) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_ABBREVIATION, abbreviation);
    SubjectResponse subjectResponse = subjectService.getByAbbreviation(abbreviation);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_ABBREVIATION, Utility.getJson(subjectResponse));
    return new ResponseEntity<>(subjectResponse, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_PROFILE_TYPE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              array = @ArraySchema(schema = @Schema(implementation = SubjectResponse.class)))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_PROFILE_TYPE)
  public ResponseEntity<List<SubjectResponse>> findByProfileType(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long profileType) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_PROFILE_TYPE, profileType);
    List<SubjectResponse> subjectResponses = subjectService.getByProfileType(profileType);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_PROFILE_TYPE, Utility.getJson(subjectResponses));
    return new ResponseEntity<>(subjectResponses, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.QUERY_OPERATION_FIND_BY_TEACHER_ID)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_OK,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              array = @ArraySchema(schema = @Schema(implementation = SubjectResponse.class)))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @GetMapping(value = SubjectProperties.QUERY_FIND_BY_TEACHER_ID)
  public ResponseEntity<List<SubjectResponse>> findByTeacherId(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long teacherId) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_TEACHER_ID, teacherId);
    List<SubjectResponse> subjectResponses = subjectService.getByTeacherId(teacherId);
    log.info(LoggingFormats.RESPONSE_OUTGOING, uuid,
        SubjectProperties.QUERY_OPERATION_FIND_BY_TEACHER_ID, Utility.getJson(subjectResponses));
    return new ResponseEntity<>(subjectResponses, HttpStatus.OK);
  }

  @Operation(summary = SubjectProperties.DELETE_OPERATION_BY_ID)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NO_CONTENT,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = Void.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @DeleteMapping(value = SubjectProperties.DELETE_BY_ID)
  public ResponseEntity<Void> deleteById(
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid,
      @PathVariable long subjectId) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid, SubjectProperties.DELETE_OPERATION_BY_ID,
        subjectId);
    subjectService.deleteById(subjectId);
    log.info(LoggingFormats.RESPONSE_DELETE_OUTGOING, uuid);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @Operation(summary = SubjectProperties.DROP_OPERATION_BY_ID)
  @ApiResponses(value = {
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NO_CONTENT,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = Void.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_BAD_REQUEST,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNAUTHORIZED,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_FORBIDDEN,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_NOT_FOUND,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))}),
      @ApiResponse(responseCode = ApiConstants.HTTP_STATUS_CODE_SERVICE_UNAVAILABLE,
          content = {@Content(mediaType = ApiConstants.MEDIA_TYPE_CONTENT_TYPE,
              schema = @Schema(implementation = ErrorResponse.class))})})
  @DeleteMapping(value = SubjectProperties.DROP_BY_ID)
  public ResponseEntity<Void> dropById(@PathVariable long subjectId,
      @RequestHeader(name = ApiConstants.UUID_HEADER, required = true) String uuid) {
    log.info(LoggingFormats.NEW_REQUEST_INCOMMING, uuid, SubjectProperties.DROP_OPERATION_BY_ID,
        subjectId);
    subjectService.dropById(subjectId);
    log.info(LoggingFormats.RESPONSE_DROP_OUTGOING, uuid);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
