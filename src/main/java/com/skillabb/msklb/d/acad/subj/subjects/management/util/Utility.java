package com.skillabb.msklb.d.acad.subj.subjects.management.util;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.LoggingFormats;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utility {

  public static boolean isEmpty(String str) {
    return str == null || SubjectConstants.EMPTY.equals(str);
  }

  public static String getJson(Object obj) {
    String json = SubjectConstants.EMPTY;
    try {
      ObjectMapper objectMapper = new ObjectMapper().registerModule(new JavaTimeModule())
          .configure(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS, false)
          .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
      json = objectMapper.writeValueAsString(obj);
    } catch (Exception ex) {
      log.error(LoggingFormats.ERROR_PARSE_OBJECT_TO_JSON, ex.getMessage());
    }
    return json;
  }

  public static Timestamp convertZonedDateTimeToTimeStamp(ZonedDateTime zonedDateTime) {
    return Timestamp.valueOf(zonedDateTime.toLocalDateTime());
  }

}
