package com.skillabb.msklb.d.acad.subj.subjects.management.constant;

import org.springframework.http.MediaType;

public class ApiConstants {
  
  public static final String UUID_HEADER = "uuid";

  public static final String HTTP_STATUS_CODE_OK = "200";
  public static final String HTTP_STATUS_CODE_CREATED = "201";
  public static final String HTTP_STATUS_CODE_NO_CONTENT = "204";

  public static final String HTTP_STATUS_CODE_BAD_REQUEST = "400";
  public static final String HTTP_STATUS_CODE_UNAUTHORIZED = "401";
  public static final String HTTP_STATUS_CODE_FORBIDDEN = "403";
  public static final String HTTP_STATUS_CODE_NOT_FOUND = "404";
  public static final String HTTP_STATUS_CODE_UNPROCESSABLE_ENTITY = "422";
  public static final String HTTP_STATUS_CODE_INTERNAL_SERVER_ERROR = "500";
  public static final String HTTP_STATUS_CODE_SERVICE_UNAVAILABLE = "503";

  public static final String MEDIA_TYPE_CONTENT_TYPE = MediaType.APPLICATION_JSON_VALUE;

  public static final String FORMAT_ERROR_RESPONSE = "yyyy-MM-dd'T'HH:mm:ssZ";
  
  public static final String COMMA_SEPARATOR = ",";
  
  private ApiConstants() {}
}
