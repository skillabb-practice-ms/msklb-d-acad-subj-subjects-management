package com.skillabb.msklb.d.acad.subj.subjects.management.service;

import java.util.List;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.SubjectResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.UpdateSubjectRequest;

public interface SubjectService {

  SubjectResponse saveSubject(AddSubjectRequest addSubjectRequest);

  SubjectResponse updateSubject(long subjectId, UpdateSubjectRequest updateSubjectRequest);

  List<SubjectResponse> getAllSubjects();

  SubjectResponse getById(long subjectId);

  SubjectResponse getByName(String name);

  List<SubjectResponse> getbyStatus(long status);

  SubjectResponse getByAbbreviation(String abbreviation);

  List<SubjectResponse> getByProfileType(long profileType);

  List<SubjectResponse> getByTeacherId(long teacherId);

  void deleteById(long subjectId);

  void dropById(long subjectId);

}
