package com.skillabb.msklb.d.acad.subj.subjects.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;

@ComponentScan(basePackages = SubjectConstants.COMPONENT_SCAN)
@SpringBootApplication
@EnableEurekaClient
public class SubjectsManagementApp {

  public static void main(String[] args) {
    SpringApplication.run(SubjectsManagementApp.class, args);
  }

}
