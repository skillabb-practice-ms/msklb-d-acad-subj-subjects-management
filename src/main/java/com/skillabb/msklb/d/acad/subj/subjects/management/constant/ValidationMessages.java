package com.skillabb.msklb.d.acad.subj.subjects.management.constant;

public class ValidationMessages {
  
  public static final String MUST_BE_NOT_NULL = "Must be not null";
  public static final String MUST_BE_NOT_EMPTY = "Must be not empty";
  public static final String SIZE_MIN_X_MAX_X = "The size must be between {min} and {max}";
  
  public static final String POSITIVE_VALUE = "Must be a positive number greater than 0";

  
  private ValidationMessages() {}
}
