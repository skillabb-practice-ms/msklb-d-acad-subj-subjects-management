package com.skillabb.msklb.d.acad.subj.subjects.management.util;

import com.skillabb.msklb.d.acad.subj.subjects.management.constant.SubjectConstants;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import java.time.ZonedDateTime;

@TestInstance(Lifecycle.PER_CLASS)
class UtilityTest {
  
  private static final String STRING_MOCK = "UT";
  private static final long LONG_MOCK = 10l;
  
  private AddSubjectRequest addSubjectRequest;
  
  @BeforeAll
  void setUp() {
    addSubjectRequest = new AddSubjectRequest();
    addSubjectRequest.setAbbreviation(STRING_MOCK);
    addSubjectRequest.setCreatedAt(ZonedDateTime.now());
    addSubjectRequest.setCreatedBy(LONG_MOCK);
    addSubjectRequest.setHours(LONG_MOCK);
    addSubjectRequest.setName(STRING_MOCK);
    addSubjectRequest.setProfileType(LONG_MOCK);
    addSubjectRequest.setSchedule(STRING_MOCK);
    addSubjectRequest.setStatus(LONG_MOCK);
    addSubjectRequest.setTeacherId(LONG_MOCK);
  }
  
  @Test
  void isEmptyNullTest() {
    Assertions.assertTrue(Utility.isEmpty(null));
  }
  
  @Test
  void isEmptyEmptyTest() {
    Assertions.assertTrue(Utility.isEmpty(SubjectConstants.EMPTY));
  }
  
  @Test
  void isEmptyContentTest() {
    Assertions.assertFalse(Utility.isEmpty(SubjectConstants.ABBREVIATION_COLUMN));
  }
  
  @Test
  void getJsonTest() {
    Assertions.assertNotNull(Utility.getJson(addSubjectRequest));
  }
  
  @Test
  void convertZonedDateTimeToTimeStampTest() {
    Assertions.assertNotNull(Utility.convertZonedDateTimeToTimeStamp(ZonedDateTime.now()));
  }
  
}
