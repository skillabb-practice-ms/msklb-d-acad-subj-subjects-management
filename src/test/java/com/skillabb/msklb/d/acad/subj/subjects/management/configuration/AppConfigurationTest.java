package com.skillabb.msklb.d.acad.subj.subjects.management.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

@TestInstance(Lifecycle.PER_CLASS)
class AppConfigurationTest {
  
  @InjectMocks
  private AppConfiguration appConfiguration;
  
  @BeforeAll
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }
  
  @Test
  void getModelMapperTest() {
    Assertions.assertNotNull(appConfiguration.getModelMapper());
  }
  
}
