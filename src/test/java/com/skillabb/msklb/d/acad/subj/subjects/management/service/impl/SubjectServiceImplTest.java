package com.skillabb.msklb.d.acad.subj.subjects.management.service.impl;

import com.skillabb.msklb.d.acad.subj.subjects.management.entity.SubjectEntity;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.SubjectResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.UpdateSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.repository.SubjectRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
class SubjectServiceImplTest {
  
  private static final String STRING_MOCK = "UT";
  private static final long LONG_MOCK = 10l;
  
  @InjectMocks
  private SubjectServiceImpl subjectServiceImpl;
  
  @Mock
  private ModelMapper modelMapper;
  @Mock
  private SubjectRepository subjectRepository;
  
  private AddSubjectRequest addSubjectRequest;
  private UpdateSubjectRequest updateSubjectRequest;
  private SubjectResponse subjectResponse;
  private SubjectEntity subjectEntity;
  
  
  @BeforeAll
  void setUp() {
    MockitoAnnotations.openMocks(this);
    
    addSubjectRequest = new AddSubjectRequest();
    addSubjectRequest.setAbbreviation(STRING_MOCK);
    addSubjectRequest.setCreatedAt(ZonedDateTime.now());
    addSubjectRequest.setCreatedBy(LONG_MOCK);
    addSubjectRequest.setHours(LONG_MOCK);
    addSubjectRequest.setName(STRING_MOCK);
    addSubjectRequest.setProfileType(LONG_MOCK);
    addSubjectRequest.setSchedule(STRING_MOCK);
    addSubjectRequest.setStatus(LONG_MOCK);
    addSubjectRequest.setTeacherId(LONG_MOCK);

    subjectResponse = new SubjectResponse();
    subjectResponse.setAbbreviation(STRING_MOCK);
    subjectResponse.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectResponse.setCreatedBy(LONG_MOCK);
    subjectResponse.setHours(LONG_MOCK);
    subjectResponse.setLastModifyAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectResponse.setLastModifyBy(LONG_MOCK);
    subjectResponse.setName(STRING_MOCK);
    subjectResponse.setProfileType(LONG_MOCK);
    subjectResponse.setSchedule(STRING_MOCK);
    subjectResponse.setStatus(LONG_MOCK);
    subjectResponse.setSubjectId(LONG_MOCK);
    subjectResponse.setTeacherId(LONG_MOCK);

    updateSubjectRequest = new UpdateSubjectRequest();
    updateSubjectRequest.setAbbreviation(STRING_MOCK);
    updateSubjectRequest.setHours(LONG_MOCK);
    updateSubjectRequest.setLastModifyAt(ZonedDateTime.now());
    updateSubjectRequest.setLastModifyBy(LONG_MOCK);
    updateSubjectRequest.setName(STRING_MOCK);
    updateSubjectRequest.setProfileType(LONG_MOCK);
    updateSubjectRequest.setSchedule(STRING_MOCK);
    updateSubjectRequest.setStatus(LONG_MOCK);
    updateSubjectRequest.setTeacherId(LONG_MOCK);
    
    subjectEntity = new SubjectEntity();
    subjectEntity.setAbbreviation(STRING_MOCK);
    subjectEntity.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectEntity.setCreatedBy(LONG_MOCK);
    subjectEntity.setHours(LONG_MOCK);
    subjectEntity.setLastModifyAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectEntity.setLastModifyBy(LONG_MOCK);
    subjectEntity.setName(STRING_MOCK);
    subjectEntity.setProfileType(LONG_MOCK);
    subjectEntity.setSchedule(STRING_MOCK);
    subjectEntity.setStatus(LONG_MOCK);
    subjectEntity.setSubjectId(LONG_MOCK);
    subjectEntity.setTeacherId(LONG_MOCK);
  }
  
  @Test
  void saveSubjectTest() {
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectEntity).thenReturn(subjectResponse);
    Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subjectEntity);
    Assertions.assertNotNull(subjectServiceImpl.saveSubject(addSubjectRequest));
  }
  
  @Test
  void updateSubjectTest() {
    Mockito.when(subjectRepository.findBySubjectId(Mockito.anyLong())).thenReturn(subjectEntity);
    Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subjectEntity);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.updateSubject(LONG_MOCK, updateSubjectRequest));
  }
  
  @Test
  void getAllSubjectsTest() {
    List<SubjectEntity> subjectEntities = Arrays.asList(subjectEntity);
    Mockito.when(subjectRepository.findAll()).thenReturn(subjectEntities);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getAllSubjects());
  }
  
  @Test
  void getByIdTest() {
    Mockito.when(subjectRepository.findBySubjectId(Mockito.anyLong())).thenReturn(subjectEntity);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getById(LONG_MOCK));
  }
  
  @Test
  void getByIdNameTest() {
    Mockito.when(subjectRepository.findByName(Mockito.anyString())).thenReturn(subjectEntity);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getByName(STRING_MOCK));
  }
  
  @Test
  void getByStatusTest() {
    List<SubjectEntity> subjectEntities = Arrays.asList(subjectEntity);
    Mockito.when(subjectRepository.findByStatus(Mockito.anyLong())).thenReturn(subjectEntities);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getbyStatus(LONG_MOCK));
  }
  
  @Test
  void getByAbbreviationTest() {
    Mockito.when(subjectRepository.findByAbbreviation(Mockito.anyString())).thenReturn(subjectEntity);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getByAbbreviation(STRING_MOCK));
  }
  
  @Test
  void getByProfileTypeTest() {
    List<SubjectEntity> subjectEntities = Arrays.asList(subjectEntity);
    Mockito.when(subjectRepository.findByProfileType(Mockito.anyLong())).thenReturn(subjectEntities);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getByProfileType(LONG_MOCK));
  }
  
  @Test
  void getByTeacherIdTest() {
    List<SubjectEntity> subjectEntities = Arrays.asList(subjectEntity);
    Mockito.when(subjectRepository.findByTeacherId(Mockito.anyLong())).thenReturn(subjectEntities);
    Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectServiceImpl.getByTeacherId(LONG_MOCK));
  }
  
  @Test
  void deleteByIdTest() {
    Mockito.when(subjectRepository.findBySubjectId(LONG_MOCK)).thenReturn(subjectEntity);
    Mockito.when(subjectRepository.save(Mockito.any())).thenReturn(subjectEntity);
    Assertions.assertDoesNotThrow(() -> subjectServiceImpl.deleteById(LONG_MOCK));
  }
  
  @Test
  void dropByIdTest() {
    Mockito.doNothing().when(subjectRepository).deleteById(Mockito.anyLong());
    Assertions.assertDoesNotThrow(() -> subjectServiceImpl.dropById(LONG_MOCK));
  }
}
