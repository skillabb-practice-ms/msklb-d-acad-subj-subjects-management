package com.skillabb.msklb.d.acad.subj.subjects.management.controller;

import com.skillabb.msklb.d.acad.subj.subjects.management.model.AddSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.SubjectResponse;
import com.skillabb.msklb.d.acad.subj.subjects.management.model.UpdateSubjectRequest;
import com.skillabb.msklb.d.acad.subj.subjects.management.service.impl.SubjectServiceImpl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@TestInstance(Lifecycle.PER_CLASS)
class SubjectControllerTest {

  private static final String STRING_MOCK = "UT";
  private static final long LONG_MOCK = 10l;

  @InjectMocks
  private SubjectController subjectController;

  @Mock
  private SubjectServiceImpl subjectServiceImpl;

  private AddSubjectRequest addSubjectRequest;
  private SubjectResponse subjectResponse;
  private UpdateSubjectRequest updateSubjectRequest;
  private String uuid = null;

  @BeforeAll
  void setUp() {
    MockitoAnnotations.openMocks(this);

    addSubjectRequest = new AddSubjectRequest();
    addSubjectRequest.setAbbreviation(STRING_MOCK);
    addSubjectRequest.setCreatedAt(ZonedDateTime.now());
    addSubjectRequest.setCreatedBy(LONG_MOCK);
    addSubjectRequest.setHours(LONG_MOCK);
    addSubjectRequest.setName(STRING_MOCK);
    addSubjectRequest.setProfileType(LONG_MOCK);
    addSubjectRequest.setSchedule(STRING_MOCK);
    addSubjectRequest.setStatus(LONG_MOCK);
    addSubjectRequest.setTeacherId(LONG_MOCK);

    subjectResponse = new SubjectResponse();
    subjectResponse.setAbbreviation(STRING_MOCK);
    subjectResponse.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectResponse.setCreatedBy(LONG_MOCK);
    subjectResponse.setHours(LONG_MOCK);
    subjectResponse.setLastModifyAt(Timestamp.valueOf(LocalDateTime.now()));
    subjectResponse.setLastModifyBy(LONG_MOCK);
    subjectResponse.setName(STRING_MOCK);
    subjectResponse.setProfileType(LONG_MOCK);
    subjectResponse.setSchedule(STRING_MOCK);
    subjectResponse.setStatus(LONG_MOCK);
    subjectResponse.setSubjectId(LONG_MOCK);
    subjectResponse.setTeacherId(LONG_MOCK);

    updateSubjectRequest = new UpdateSubjectRequest();
    updateSubjectRequest.setAbbreviation(STRING_MOCK);
    updateSubjectRequest.setHours(LONG_MOCK);
    updateSubjectRequest.setLastModifyAt(ZonedDateTime.now());
    updateSubjectRequest.setLastModifyBy(LONG_MOCK);
    updateSubjectRequest.setName(STRING_MOCK);
    updateSubjectRequest.setProfileType(LONG_MOCK);
    updateSubjectRequest.setSchedule(STRING_MOCK);
    updateSubjectRequest.setStatus(LONG_MOCK);
    updateSubjectRequest.setTeacherId(LONG_MOCK);

    uuid = UUID.randomUUID().toString();
  }

  @Test
  void addSubjectTest() {
    Mockito.when(subjectServiceImpl.saveSubject(Mockito.any())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectController.addSubject(uuid, addSubjectRequest));
  }

  @Test
  void updateSubjectTest() {
    Mockito.when(subjectServiceImpl.updateSubject(Mockito.anyLong(), Mockito.any()))
        .thenReturn(subjectResponse);
    Assertions
        .assertNotNull(subjectController.updateSubject(uuid, LONG_MOCK, updateSubjectRequest));
  }

  @Test
  void findAllTest() {
    List<SubjectResponse> subjectResponses = new ArrayList<>();
    subjectResponses.add(subjectResponse);
    Mockito.when(subjectServiceImpl.getAllSubjects()).thenReturn(subjectResponses);
    Assertions.assertNotNull(subjectController.findAll(uuid));
  }

  @Test
  void findByIdTest() {
    Mockito.when(subjectServiceImpl.getById(Mockito.anyLong())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectController.findById(uuid, LONG_MOCK));
  }

  @Test
  void findByNameTest() {
    Mockito.when(subjectServiceImpl.getByName(Mockito.anyString())).thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectController.findByName(uuid, STRING_MOCK));
  }

  @Test
  void findByStatusTest() {
    List<SubjectResponse> subjectResponses = Arrays.asList(subjectResponse);
    Mockito.when(subjectServiceImpl.getbyStatus(Mockito.anyLong())).thenReturn(subjectResponses);
    Assertions.assertNotNull(subjectController.findByStatus(uuid, LONG_MOCK));
  }

  @Test
  void findByAbbreviationTest() {
    Mockito.when(subjectServiceImpl.getByAbbreviation(Mockito.anyString()))
        .thenReturn(subjectResponse);
    Assertions.assertNotNull(subjectController.findByName(uuid, STRING_MOCK));
  }

  @Test
  void findByProfileTypeTest() {
    List<SubjectResponse> subjectResponses = Arrays.asList(subjectResponse);
    Mockito.when(subjectServiceImpl.getByProfileType(Mockito.anyLong()))
        .thenReturn(subjectResponses);
    Assertions.assertNotNull(subjectController.findByStatus(uuid, LONG_MOCK));
  }

  @Test
  void findByTeacherIdTest() {
    List<SubjectResponse> subjectResponses = Arrays.asList(subjectResponse);
    Mockito.when(subjectServiceImpl.getByTeacherId(Mockito.anyLong())).thenReturn(subjectResponses);
    Assertions.assertNotNull(subjectController.findByStatus(uuid, LONG_MOCK));
  }

  @Test
  void deleteByIdTest() {
    Mockito.doNothing().when(subjectServiceImpl).deleteById(Mockito.anyLong());
    Assertions.assertDoesNotThrow(() -> subjectController.deleteById(uuid, LONG_MOCK));
  }

  @Test
  void dropByIdTest() {
    Mockito.doNothing().when(subjectServiceImpl).dropById(Mockito.anyLong());
    Assertions.assertDoesNotThrow(() -> subjectController.dropById(LONG_MOCK, uuid));
  }

}
